(async () => {

  const importObject = {
    imports: {
      log: arg => console.log(arg)
    }
  }

  const file = await fetch('hello.wasm')
  const obj = await WebAssembly.instantiateStreaming(file, importObject)

  console.log('---')
  obj.instance.exports.exported_func()

  console.log('---')
  obj.instance.exports.loop_test()

  console.log('---')
  obj.instance.exports.loop_test2()

  console.log('---')
  obj.instance.exports.loop_test3()

  console.log('fin', obj)
  window.obj = obj

})()
