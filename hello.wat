(module
  (func $log (import "imports" "log")
    (param i32))

  ;; export to js
  (func (export "exported_func")
    (call $log (i32.const 88)))

  (func (export "loop_test")
    (local $i i32)
    (local $size i32)

    (set_local $size (i32.const 5))

    (block $exit
      (loop $loop
        ;; if (i >= size) break
        (if (i32.ge_u (get_local $i) (get_local $size)) (br $exit))
        ;; console.log(i)
        (call $log (get_local $i))
        ;; i++
        (set_local $i (i32.add (get_local $i) (i32.const 1)))
        ;; go back to geginning of the loop with $loop label
        (br $loop))))

  (func (export "loop_test2")
    (local $i i32)
    (set_local $i (i32.const 5))

    (block $exit
      (loop $loop
        (set_local $i (i32.sub (get_local $i) (i32.const 1))) ;; i--
        (call $log (get_local $i))                            ;; log i
        (br_if $exit (i32.le_s (get_local $i) (i32.const 0))) ;; if i<=0 break
        (br $loop)))                                          ;; loop
    )

  (func (export "loop_test3")
    (local $i i32)

    i32.const 5
    set_local $i

    (block
      (loop
        get_local $i ;; put value of $i to the stack
        i32.const 1  ;; put value of 1 to the stack
        i32.sub      ;; substract 2 stack values and put result on top
        set_local $i ;; get value from stack and use it as new $i value

        get_local $i ;; put value of $i to the stack
        call $log    ;; call $log function with top stack value

        get_local $i ;; put value of $i to the stack
        i32.const 0  ;; put value of 0 to the stack
        i32.le_s     ;; copare if lest or equal (signed) last 2 stack values
        br_if 1      ;; if top stack value is true then branch to second block
                     ;; the (block) as a way to exit loop

        br 0         ;; branch to first block (loop)
        ))
    )

  )
